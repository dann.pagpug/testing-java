1. untuk menjalankan upload file diperluakan tools Postman sebagai alat bantu.

##  upload csv Siswa / Nilai: 
2. pada tools postman 
	* masukan url : http://localhost:8081/api/uploadFile,
	* http method : POST
	* form-data : checked
	* pada bagian field Key isikan : file
	* pada bagian value isikan : file yang di upload
3. klik button send.

## Update Nilai
4. pada tool Postman : 
	* isikan url : 	http://localhost:8081/api/nilai
	* http method : PUT
	* Pilih Tab Body
	* raw : checked
	* input type : JSON(application/json)
	* contoh Body : 
	{
        "id": 100,
        "siswaId": 68,
        "mataPelajaran": "matematika",
        "nilai": 80
    }
	* klik buton send.

## delete mataPelajaran
5. pada tool postman : 
	* isikan url : http://localhost:8081/api/deletes
	* http method : GET
	* klik button send.

## melihat rapor dan filtering
6. pada tool postman : 
	* http method : GET
	## melihat list rapor 
		* isikan url : http://localhost:8081/api/rapor
	## filtering by nama :
		* isikan url : http://localhost:8081/api/rapor/nama/{keyword}
			contoh : http://localhost:8081/api/rapor/nama/daniel
	## filtering by No induk :
		* isikan url : localhost:8081/api/rapor/nomorinduk/{keyword}
			contoh :localhost:8081/api/rapor/nomorinduk/001
	## filtering by mataPelajaran :
		* isikan url : localhost:8081/api/rapor/mataPelajaran/{keyword}
			contoh :localhost:8081/api/rapor/mataPelajaran/sejarah
	## filtering by nilai :
		* isikan url : localhost:8081/api/rapor/nilai/{keyword}
			contoh :localhost:8081/api/rapor/nilai/80