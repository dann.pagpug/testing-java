package com.javatest.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.javatest.demo.domain.Nilai;

@Repository
public interface NilaiRepository extends JpaRepository<Nilai, Long> {
	@Query("SELECT r FROM Nilai r WHERE r.id = :id")
	Nilai findNilaiById(@Param("id") long id);
}
