package com.javatest.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.javatest.demo.domain.Siswa;

@Repository
public interface SiswaRepository extends JpaRepository<Siswa, Long>{

	@Query("SELECT siswa FROM Siswa siswa WHERE siswa.id = :id")
	Siswa findOneById(@Param("id") Long id);
	
	@Query("SELECT siswa FROM Siswa siswa WHERE siswa.nomorInduk = :noInduk")
	Siswa findOneByNoInduk(@Param("noInduk") String noInduk);
	
	@Query("SELECT siswa FROM Siswa siswa WHERE lower(siswa.nama) LIKE %:nama% ")
	List<Siswa> findByName(@Param("nama") String nama);
	
	@Query("SELECT siswa FROM Siswa siswa WHERE lower(siswa.nomorInduk) LIKE %:noInduk% ")
	List<Siswa> findByNoInduk(@Param("noInduk") String noInduk);
	
	@Query("SELECT siswa FROM Siswa siswa, "
			+ "Nilai r "
			+ " WHERE lower(r.mataPelajaran) LIKE %:mataPelajaran% "
			+ "AND  r.siswaId = siswa.id")
	List<Siswa> findByMataPelajaran(@Param("mataPelajaran") String mataPelajaran);
	
	@Query("SELECT siswa FROM Siswa siswa, "
			+ "Nilai r "
			+ " WHERE r.nilai = :nilai "
			+ "AND  r.siswaId = siswa.id")
	List<Siswa> findByNilai(@Param("nilai") long nilai);
	
}
