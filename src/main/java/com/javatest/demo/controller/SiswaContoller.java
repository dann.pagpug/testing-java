package com.javatest.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javatest.demo.domain.Siswa;
import com.javatest.demo.repository.SiswaRepository;

@RestController
@RequestMapping("/api")
public class SiswaContoller {

	@Autowired SiswaRepository service;
	
	@GetMapping("/siswas")
	public ResponseEntity<List<Siswa>>getAll(){
		List<Siswa> list = service.findAll();
		return new ResponseEntity<List<Siswa>>(list, HttpStatus.OK);
	}
	
	@GetMapping("/deletes")
	public ResponseEntity<String>deleteAll(){
		 service.deleteAll();
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
}
