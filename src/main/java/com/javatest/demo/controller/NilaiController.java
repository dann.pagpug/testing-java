package com.javatest.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javatest.demo.domain.Nilai;
import com.javatest.demo.service.NilaiService;

@RestController
@RequestMapping("/api")
public class NilaiController {

	@Autowired NilaiService service;
	
	@PutMapping("/nilai")
	public ResponseEntity<String>update(@RequestBody Nilai m){
		
		service.update(m);
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
	
	@GetMapping("/nilai")
	public ResponseEntity<List<Nilai>>findAll(){
		List<Nilai> listNilai = service.findAll();
		return new ResponseEntity<List<Nilai>>(listNilai, HttpStatus.OK);
	}
}
