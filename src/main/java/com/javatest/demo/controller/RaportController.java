package com.javatest.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.javatest.demo.domain.Nilai;
import com.javatest.demo.domain.Siswa;
import com.javatest.demo.repository.SiswaRepository;
import com.javatest.demo.service.SiswaService;
import com.javatest.demo.viewModel.RaportViewModel;

@RestController
@RequestMapping("/api")
public class RaportController {

	@Autowired SiswaService siswaService;
	@Autowired SiswaRepository siswaRepo;
	
	@GetMapping("/rapors")
	public ResponseEntity<List<RaportViewModel>> findRaportByNoInduk(@RequestParam String noInduk) throws Exception{
		
		List<RaportViewModel> listVm = new ArrayList<RaportViewModel>();
		
		try {
			Siswa siswa = siswaService.findOneByNoInduk(noInduk);
			for(Nilai a : siswa.getNilais()) {
				listVm.add(new RaportViewModel(siswa.getNomorInduk(), siswa.getNama(), a.getMataPelajaran(), a.getNilai()));
			}
		} catch (Exception e) {
			throw new Exception();
		}
		return new ResponseEntity<List<RaportViewModel>>(listVm, HttpStatus.OK);
	}
	
	@GetMapping("/rapor/nama/{nama}")
	public ResponseEntity<List<RaportViewModel>> findRaportByName(@PathVariable String nama) throws Exception{
		
		List<RaportViewModel> listVm = new ArrayList<RaportViewModel>();
		
		try {
			List<Siswa> siswas = siswaService.findByName(nama);
			
			if(siswas.size()>0) {
				for(Siswa std : siswas) {
					if(std.getNilais().size() > 0) {
						for(Nilai a : std.getNilais()) {
							listVm.add(new RaportViewModel(std.getNomorInduk(), std.getNama(), a.getMataPelajaran(), a.getNilai()));
						}
					}
				}
			}
			
		} catch (Exception e) {
			throw new Exception();
		}
		return new ResponseEntity<List<RaportViewModel>>(listVm, HttpStatus.OK);
	}
	
	@GetMapping("/rapor/nomorinduk/{noInduk}")
	public ResponseEntity<List<RaportViewModel>> findByNoInduk(@PathVariable String noInduk) throws Exception{
		
		List<RaportViewModel> listVm = new ArrayList<RaportViewModel>();
		
		try {
			List<Siswa> siswas = siswaService.findByNoInduk(noInduk);
			
			if(siswas.size()>0) {
				for(Siswa std : siswas) {
					if(std.getNilais().size() > 0) {
						for(Nilai a : std.getNilais()) {
							listVm.add(new RaportViewModel(std.getNomorInduk(), std.getNama(), a.getMataPelajaran(), a.getNilai()));
						}
					}
				}
			}
			
		} catch (Exception e) {
			throw new Exception();
		}
		return new ResponseEntity<List<RaportViewModel>>(listVm, HttpStatus.OK);
	}
	
	@GetMapping("/rapor/matapelajaran/{mataPelajaran}")
	public ResponseEntity<List<RaportViewModel>> mataPelajaran(@PathVariable String mataPelajaran) throws Exception{
		
		List<RaportViewModel> listVm = new ArrayList<RaportViewModel>();
		
		try {
			List<Siswa> siswas = siswaService.findByMataPelajaran(mataPelajaran);
			
			if(siswas.size()>0) {
				for(Siswa std : siswas) {
					if(std.getNilais().size() > 0) {
						for(Nilai a : std.getNilais()) {
							listVm.add(new RaportViewModel(std.getNomorInduk(), std.getNama(), a.getMataPelajaran(), a.getNilai()));
						}
					}
				}
			}
			
		} catch (Exception e) {
			throw new Exception();
		}
		return new ResponseEntity<List<RaportViewModel>>(listVm, HttpStatus.OK);
	}
	
	@GetMapping("/rapor/nilai/{nilai}")
	public ResponseEntity<List<RaportViewModel>> findByNilai(@PathVariable long nilai) throws Exception{
		
		List<RaportViewModel> listVm = new ArrayList<RaportViewModel>();
		
		try {
			List<Siswa> siswas = siswaService.findByNilai(nilai);
			
			if(siswas.size()>0) {
				for(Siswa std : siswas) {
					if(std.getNilais().size() > 0) {
						for(Nilai a : std.getNilais()) {
							listVm.add(new RaportViewModel(std.getNomorInduk(), std.getNama(), a.getMataPelajaran(), a.getNilai()));
						}
					}
				}
			}
			
		} catch (Exception e) {
			throw new Exception();
		}
		return new ResponseEntity<List<RaportViewModel>>(listVm, HttpStatus.OK);
	}
	
	@GetMapping("/rapor")
	public ResponseEntity<List<RaportViewModel>> findAll() throws Exception{
		
		List<RaportViewModel> listVm = new ArrayList<RaportViewModel>();
		
		try {
			List<Siswa> siswas = siswaService.findAll();
			
			if(siswas.size()>0) {
				for(Siswa std : siswas) {
					if(std.getNilais().size() > 0) {
						for(Nilai a : std.getNilais()) {
							listVm.add(new RaportViewModel(std.getNomorInduk(), std.getNama(), a.getMataPelajaran(), a.getNilai()));
						}
					}
				}
			}
			
		} catch (Exception e) {
			throw new Exception();
		}
		return new ResponseEntity<List<RaportViewModel>>(listVm, HttpStatus.OK);
	}
}
