package com.javatest.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.javatest.demo.domain.Nilai;
import com.javatest.demo.domain.Siswa;
import com.javatest.demo.repository.NilaiRepository;
import com.javatest.demo.repository.SiswaRepository;
import com.javatest.demo.service.FileUploadService;
import com.javatest.demo.service.NilaiService;
import com.javatest.demo.service.SiswaService;

@RestController
//@CrossOrigin("http://localhost:8081")
@RequestMapping("/api")
public class UploadFileController {
	@Autowired FileUploadService service;
	@Autowired SiswaService siswaService;
	@Autowired NilaiService nilaiService;
	@Autowired SiswaRepository siswaRepo;
	@Autowired NilaiRepository nilaiRepo;
	

	@PostMapping("/uploadFile")
	public String uploadCSVFile(@RequestParam("file") MultipartFile file) throws Exception {
		String result = "Uploaded";
		byte[] bytes = file.getBytes();
        String completeData = new String(bytes);
        String[] rows = completeData.split("\n");
        String aa= rows[0].split(",")[0].toString().toLowerCase();
        String ss= rows[0].split(",")[1].toLowerCase();
        String cc= rows[0].split(",")[2].toLowerCase();
        if(rows[0].split(",")[0].toString().toLowerCase().contains("nomor_induk") && 
        		rows[0].split(",")[1].toString().toLowerCase().contains("mata_pelajaran")&&
        		rows[0].split(",")[2].toLowerCase().contains("nilai")) {
        	for(String a : rows) {
        		if(a.toLowerCase().contains("nomor_induk,mata_pelajaran,nilai")) {
        			
        		}else {
        			try {
                		Siswa siswa = siswaService.findOneByNoInduk(a.split(",")[0]);
        	        	Nilai nilai = new Nilai();
        	        	
        	        	nilai.setSiswaId(siswa.getId());
        	        	nilai.setMataPelajaran(a.split(",")[1]);
        	        	nilai.setNilai(new Long(a.split(",")[2]));
        	        	
        	        	nilaiService.save(nilai);
    				} catch (Exception e) {
    					throw new Exception("Siswa tidak terdaftar!");
    				}
        		}
        		
        	}
        }else if(rows[0].split(",")[0].toLowerCase().contains("nomor_induk") && 
        		rows[0].split(",")[1].toLowerCase().contains("nama")&&
        		rows[0].split(",")[2].toLowerCase().contains("kelas")) {
        	Siswa siswa = new Siswa();
        	try {
        		for(String a : rows) {
        			if(a.toLowerCase().contains("nomor_induk,nama,kelas")) {
        				
        			}else {
        				Siswa check = siswaRepo.findOneByNoInduk(a.split(",")[0]);
        				if(check == null) {
	        				siswa.setNomorInduk(a.split(",")[0]);
	    					siswa.setNama(a.split(",")[1]);
	    					siswa.setKelas(a.split(",")[2]);
	    					
	    					siswaService.save(siswa);
        				}
        			}
					
				}
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
        }else {
        	result = "format file salah!";
        }
//        List<Siswa> aaa = CsvUtils.read(Siswa.class, file.getInputStream());
		 return result;
	}
}
