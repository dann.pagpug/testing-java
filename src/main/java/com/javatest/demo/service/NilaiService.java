package com.javatest.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javatest.demo.domain.Nilai;
import com.javatest.demo.repository.NilaiRepository;

@Service
public class NilaiService {

	@Autowired NilaiRepository repo;
	
	public Nilai save(Nilai m) {
		Nilai nilai = new Nilai();
		
		nilai.setMataPelajaran(m.getMataPelajaran());
		nilai.setNilai(m.getNilai());
		nilai.setSiswaId(m.getSiswaId());
		
		 repo.save(nilai);
		return nilai;
	}
	
	public Nilai findOneById(long id) {
		Nilai nilai = new Nilai();
		try {
			nilai = repo.findNilaiById(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return nilai;
	}
	
	public List<Nilai> findAll(){
		List<Nilai> list = new ArrayList<Nilai>();
		try {
			list = repo.findAll();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}
	
	public void update(Nilai m) {
		Nilai nilai = new Nilai();
		try {
			nilai = repo.findNilaiById(m.getId());
			nilai.setMataPelajaran(m.getMataPelajaran());
			nilai.setNilai(m.getNilai());
			nilai.setSiswaId(m.getSiswaId());
			
			repo.save(nilai);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void delete(Long id) {
		repo.deleteById(id);
	}
}
