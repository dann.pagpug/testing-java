package com.javatest.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javatest.demo.domain.Siswa;
import com.javatest.demo.repository.SiswaRepository;

@Service
public class SiswaService {
	@Autowired SiswaRepository repo;
	
	public Siswa save(Siswa m) {
		Siswa siswa = new Siswa();
		
		siswa.setNama(m.getNama());
		siswa.setKelas(m.getKelas());
		siswa.setNomorInduk(m.getNomorInduk());
		
		repo.save(siswa);
		
		return siswa;
	}
	public Siswa update(Siswa m) {
		Siswa siswa = repo.findOneById(m.getId());
		
		siswa.setNama(m.getNama());
		siswa.setKelas(m.getKelas());
		siswa.setNomorInduk(m.getNomorInduk());
		
		siswa = repo.saveAndFlush(siswa);
		return siswa;
	}
	
	public Siswa findOneByNoInduk(String noInduk) {
		Siswa siswa = new Siswa();
		try {
			siswa = repo.findOneByNoInduk(noInduk);
		} catch (Exception e) {
			System.out.println(e);
		}
		return siswa;
	}
	
	public List<Siswa> findByName(String nama) {
		
		return repo.findByName(nama.toLowerCase());
	}
	
	public List<Siswa> findByNoInduk(String noInduk) {
		
		return repo.findByNoInduk(noInduk.toLowerCase());
	}
	
	public List<Siswa> findByMataPelajaran(String mataPelajaran) {
		
		return repo.findByMataPelajaran(mataPelajaran.toLowerCase());
	}
	
	public List<Siswa> findByNilai(long nilai) {
		
		return repo.findByNilai(nilai);
	}
	
public List<Siswa> findAll() {
		
		return repo.findAll();
	}
}
