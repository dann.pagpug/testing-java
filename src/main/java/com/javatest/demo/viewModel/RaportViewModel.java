package com.javatest.demo.viewModel;

public class RaportViewModel {
	
	
	private String nomorInduk;
	private String nama;
	private String mataPelajaran;
	private long nilai;
	public String getNomorInduk() {
		return nomorInduk;
	}
	public void setNomorInduk(String nomorInduk) {
		this.nomorInduk = nomorInduk;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getMataPelajaran() {
		return mataPelajaran;
	}
	public void setMataPelajaran(String mataPelajaran) {
		this.mataPelajaran = mataPelajaran;
	}
	public long getNilai() {
		return nilai;
	}
	public void setNilai(long nilai) {
		this.nilai = nilai;
	}
	
	public RaportViewModel() {
		
	}
	public RaportViewModel(String nomorInduk, String nama, String mataPalajaran, long nilai) {
		this.nomorInduk = nomorInduk;
		this.nama = nama;
		this.nilai = nilai;
		this.mataPelajaran = mataPalajaran;
	}
}
