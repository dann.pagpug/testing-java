package com.javatest.demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="nilai")
public class Nilai {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="siswa_id")
	private long siswaId;
	
	@Column(name="mata_pelajaran")
	private String mataPelajaran;
	
	@Column(name="nilai")
	private long nilai;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="siswa_id", referencedColumnName="id", insertable = false, updatable = false)
	private Siswa siswa;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(long siswaId) {
		this.siswaId = siswaId;
	}

	public long getNilai() {
		return nilai;
	}

	public void setNilai(long nilai) {
		this.nilai = nilai;
	}

	public String getMataPelajaran() {
		return mataPelajaran;
	}

	public void setMataPelajaran(String mataPelajaran) {
		this.mataPelajaran = mataPelajaran;
	}

	public Siswa getSiswa() {
		return siswa;
	}

	public void setSiswa(Siswa siswa) {
		this.siswa = siswa;
	}
	
	
}
