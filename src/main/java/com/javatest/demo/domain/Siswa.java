package com.javatest.demo.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity()
@Table(name="siswa")
public class Siswa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="nomor_induk")
	private String nomorInduk;
	
	@Column(name="nama")
	private String nama;
	
	@JsonBackReference
	@OneToMany(cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			mappedBy = "siswa")
	private List<Nilai> nilais;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
 
	public String getNomorInduk() {
		return nomorInduk;
	}

	public void setNomorInduk(String nomorInduk) {
		this.nomorInduk = nomorInduk;
	}

	@Column(name="kelas")
	private String kelas;

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public List<Nilai> getNilais() {
		return nilais;
	}

	public void setNilais(List<Nilai> nilais) {
		this.nilais = nilais;
	}
}
