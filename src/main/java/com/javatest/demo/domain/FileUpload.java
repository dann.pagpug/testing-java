package com.javatest.demo.domain;

public class FileUpload {

	private String fileName;
	private String filetype;
	private long size;
	
	public FileUpload(String fileName, String fileType, long size) {
		this.fileName = fileName;
		this.filetype = filetype;
		this.size = size;
	}
}
